const TEST = window.location.hostname == 'localhost';
const MOBILE = window.innerWidth < 768;

const LOAD_IMAGE = (src) => {
    let img = document.createElement('img');
    let promise = new Promise((res, rej) => {
        img.onload = () => { res(); };
        img.onerror = () => { rej(); };
    });
    img.src = src;

    return promise;
};
const LOAD_IMAGES = (arImages) => {
    let countAll = arImages.length,
        countReady = 0,
        resolver,
        onLoad = () => {
            countReady++;
            if(countAll === countReady){
                resolver();
            }
        };

    arImages.forEach(src => {
        LOAD_IMAGE(src).then(onLoad);
    });

    return new Promise(res => {
        resolver = res;
    });
};
class BotMonolog{
    constructor($parent){
        this.$root = $parent;

        this.speed = 500;
    }
    removeFirst(){
        let $msg = $(this.$root.find('.botMessages__msgOuter')[0]);

        $msg.addClass('removed');
        $msg.css('transition', '0s');
        $msg.slideUp({duration: this.speed});
        return new Promise((res) => { setTimeout(() => { $msg.remove(); res()}, this.speed + 100); });
    }

    removeAll(){
        return new Promise(res => { this.removePromise = res; this.removeOne(); })
    }

    removeOne(){
        let count = this.$root.find('.botMessages__msgOuter').length;
        if(count){
            this.removeFirst().then(() => { this.removeOne(); });
        }else if(this.removePromise){
            this.removePromise();
        }
    }

    calcMessagesHeightSum(){
        let $msgs = this.$root.find('.botMessages__msgOuter'),
            summ = 0;

        $msgs.each((i, el) => {
            summ += $(el).outerHeight();
        });

        return summ;
    }
    addMessage(content){
        let $msg = $(this.messageHtml(content));
        let sum = this.calcMessagesHeightSum(),
            height = this.$root.parent().height();
        $msg.css({transform: 'translateX(50%) translateY('+(height/2 - sum)+'px)'});
        this.$root.append($msg);
        return new Promise((res) => {
            setTimeout(() => {
                sum += $msg.height();
                if(sum > this.$root.parent().height()){
                    this.removeFirst();
                }

                $msg.css({transform: 'none'});
                $msg.removeClass('waiting');
                setTimeout(() => { res(); }, this.speed);
            }, 100);
        })
    }
    messageHtml(content){
        return `<div class="botMessages__msgOuter waiting" style="transition: ${this.speed}ms">
                        <div class="botMessages__msg" style="transition: ${this.speed}ms">
                            <div class="botMessage">${content}</div>
                        </div>
                    </div>`;
    }
    show(arrMessages){
        this.messages = arrMessages;
        return new Promise(res => {
            this.addLoop(0, res);
        });
    }
    addLoop(i, res){
        if(!this.messages[i]){
            res();
            return;
        }

        let text = this.messages[i].text,
            delay = this.messages[i].delay ? this.messages[i].delay : 500;

        setTimeout(() => {
            if(this.messages[i].clear){
                this.removeAll().then(() => {
                    this.addMessage(text).then(() => {
                        if(this.messages[i].onReady){
                            this.messages[i].onReady();
                        }
                        this.addLoop(i+1, res);
                    });
                });
            }else{
                this.addMessage(text).then(() => {
                    if(this.messages[i].onReady){
                        this.messages[i].onReady();
                    }
                    this.addLoop(i+1, res);
                });
            }
        }, delay);

    }
}

const START_MONOLOG_ANIMATION = () => {
    let M = new BotMonolog($('._botMessages'));
    let show = (params) => { return new Promise(res => { M.show(params).then(() => {res()}) }) };
    let loop = (func, params) => {
        func(params).then(() => { setTimeout(() => { loop(func, params);}, 2000); });
    };


    let stickers = ['/img/1.png', '/img/2.png', '/img/3.png', '/img/hotel.jpg', '/img/product_1.png', '/img/product_2.png'];
    let diagramHTML = `<div class="interview _interview">
            <div class="interview__circle">
                <div class="interview__shapes _interviewDiagram">
                    <span></span>
                    <span><span class="circle"></span><span class="txt">Круто!</span></span>
                    <span><span class="circle"></span><span class="txt">Так себе</span></span>
                </div>           
                <div class="interview__numbers">
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>`;
    let diagramAnimation = () => {
        setTimeout(() => {
            let $el = $('._interviewDiagram');
            $el.addClass('circle');
            setTimeout(() => { $el.addClass('complete'); }, 1000);
        }, 1500);
    };

    let starsSVG = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41 40">
      <path fill="#f8d548" d="M55,78L69,68l13,9L77,62l12-9H74L69,38,64,53H48l12,9Z" transform="translate(-48 -38)"/>
    </svg>`;
    let starsAnimation = () => {
        $('._stars').addClass('active');
    };

    let paymentHTML = `<div class="iphone _iphone">
            <div class="iphone__circle">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <div class="iphone__coin"></div><div class="iphone__check"></div>
            <div class="iphone__img"></div>
        </div>`;
    let paymentAnimation = () => {
        setTimeout(() => { $('._iphone').addClass('active'); }, 500);
    };

    let stickersHTML = `<div class="stickerSlider _stickerSlider">
            <div class="stickerSlider__outer">
                <div class="stickerSlider__item"><img src="img/1.png"/></div>
                <div class="stickerSlider__item"><img src="img/2.png"/></div>
                <div class="stickerSlider__item"><img src="img/3.png"/></div>
            </div>
        </div>`;
    let stickersAnimation = () => {
        let $el = $('.stickerSlider');
        setTimeout(() => {
            $el.addClass('active');
            setTimeout(() => {
                $el.addClass('complete');
            }, 1000);
        }, 500);
    };

    LOAD_IMAGES(stickers).then(() => {
        loop(show, [
            { delay: 500, text: 'Привет! Я общаюсь с клиентами и помогаю им сделать покупку.', clear: true },
            { delay: 500, text: 'Оперативно отвечаю на вопросы и всегда рад клиенту.' },
            { delay: 500, text: 'Сообщаю о статусе заказа стикерами в стиле вашей компании.' },
            { delay: 500, text: stickersHTML, onReady: stickersAnimation},
            { delay: 2500, text: 'Провожу опросы и показываю статистику ответов.', clear: true },
            { delay: 500, text: diagramHTML, onReady: diagramAnimation},
            { delay: 4500, text: 'Отправляю персональные акции.', clear: true },
            { delay: 500, text: '<em>Мы знаем, как Вы любите Тайланд. Возможно Вам понравится здесь</em> ☺' },
            { delay: 500, text: '<img src="img/hotel.jpg" style="width: 100%"/>' },
            { delay: 2500, text: 'Собираю отзывы клиентов.', clear: true },
            { delay: 500, text: '<div class="stars _stars">'+starsSVG+starsSVG+starsSVG+starsSVG+starsSVG+'</div>', onReady: starsAnimation},
            { delay: 2500, text: 'Привлекательно показываю ассортимент товаров и услуг.', clear: true },
            { delay: 500, text: '<img src="img/product_1.png" class="product"/><img class="product" src="img/product_2.png"/>' },
            { delay: 5000, text: 'Принимаю оплаты!', clear: true },
            { delay: 500, text: paymentHTML, onReady: paymentAnimation},
        ]);
    });
};
const BOT_COMES = () => {
    let $bot = $('.firstScreenBot');

    return new Promise(res => {
        LOAD_IMAGE('/img/bot_01.png').then(() => {
            setTimeout(() => {
                $bot.addClass('active');
                res();
            }, 500);
        });
    });
};
const PARALAX = ($el, set) => {
    let pos = $el.offset(),
        width = $el.width(),
        height = $el.height(),
        transition = false,
        needPause = false,
        timeout;

    let move = (dx, dy) => {
        Object.keys(set).forEach(selector => {
            let param = set[selector],
                x = param.x ? param.x : .1,
                y = param.y ? param.y : .1;
            $(selector).css({
                transform: 'translate3d('+(dx*x)+'px, '+(dy*y)+'px, 0)',
                transition: transition ? '.3s' : '0s'
            });
        });
    };
    let onMouseMove = (e) => {
        let ev = e.originalEvent,
            clientX = ev.clientX,
            clientY = ev.clientY,
            dx = clientX - (width / 2 + pos.left),
            dy = clientY - (height / 2 + pos.top);

        if (!needPause) {
            move(dx, dy);
        }
        needPause = transition;
    };
    let onResize = () => {
        pos = $el.offset();
        width = $el.width();
        height = $el.height();
    };
    let onMouseLeave = () => {
        transition = false;
        clearTimeout(timeout);
        Object.keys(set).forEach(selector => {
            $(selector).css({
                transform: 'translate3d(0px, 0px, 0)',
                transition: '.3s'
            });
        });
    };
    let onMouseOver = () => {
        transition = true;
        setTimeout(() => {
            transition = false
        }, 300);
    };

    $el.on('mousemove', onMouseMove);
    $el.on('mouseleave', onMouseLeave);
    $el.on('mouseenter', onMouseOver);
    $(window).resize(onResize);
};
const INIT_FORM = ($el) => {
    let $btn = $el.find('button'),
        $input = $el.find('input'),
        status = 'closed';

    const open = () => {
        $el.addClass('active');
        //$input.focus();
        status = 'opened';
    };
    const close = () => {
        if(status === 'success') return;

        $el.removeClass('active');
        status = 'closed';
    };
    const send = () => {

        if($input.val().length !== 18) return;

        $el.addClass('loading');
        status = 'loading';

        if(TEST){
            setTimeout(() => {
                $el.removeClass('loading');
                $el.addClass('success');
                status = 'success';
                setTimeout(() => {
                    $input.val('');
                    $el.removeClass('active success');
                    status = 'closed';
                }, 5000);
            }, 1000);
        }else{
            $.post('/form.php', {phone: $input.val()}, res => {
                $el.removeClass('loading');
                $el.addClass('success');
                status = 'success';
                setTimeout(() => {
                    $input.val('');
                    $el.removeClass('active success');
                    status = 'closed';
                }, 5000);
            });
        }
    };
    const onClickBtn = (e) => {
        e.preventDefault();
        switch (status){
            case 'closed':
                open();
                break;
            case 'opened':
                send();
                break;
        }
    };

    $input.mask("+7 (999) 999-99-99");

    $btn.on('click', onClickBtn);
    $el.on('click', (e) => { e.stopPropagation(); });
    $('body').on('click', close);
};
const INIT_FOOTER_FORM = () => {
    let $form = $('._form'),
        $btn = $form.find('button'),
        $input = $form.find('input');

    const isValid = () => {
        return $input.val().length === 18
    };

    const send = () => {
        let $val = $input.val();

        $form.addClass('loading');

        if(TEST){
            setTimeout(() => {
                $form.removeClass('loading').addClass('success');
                setTimeout(() => {
                    $input.val('');
                    $form.removeClass('success');
                }, 3000);
            }, 2000);
        }else{
            $.post('/form.php', {'phone': $val}, res => {
                console.log(res);Œ
                $form.removeClass('loading').addClass('success');
                setTimeout(() => {
                    $input.val('');
                    $form.removeClass('success');
                }, 5000);
            });
        }
    };

    const onSubmitForm = (e) => {
        e.preventDefault();

        if(isValid()){
            send();
        }
    };

    $input.mask('+7 (999) 999-99-99');
    $form.on('submit', onSubmitForm);
};

/** PAGE BLOCKS **/
const B_FIRST_SCREEN = () => {
    if(!MOBILE){
        BOT_COMES().then(() => { setTimeout(START_MONOLOG_ANIMATION, 1000)});
        PARALAX($('._firstScreen'), {
            '._par1': {x: -.05, y: -.025},
            '._par2': {x: -.025, y: -.0125},
            '._par3': {x: -.025, y: -.0125},
            '._par4': {x: -.04, y: -.02},
            '._par2_1': {x: -.02, y: -.02},
            '._par2_2': {x: -.01, y: -.01},
            '._par2_3': {x: -.05, y: -.05},
            '._par2_4': {x: -.04, y: -.04},
        });
    }

    $('._btnForm').each((i, el) => {
        INIT_FORM($(el));
    });

    $('._anchor').on('click', function(e){
        e.preventDefault();
        let top = $($(this).attr('href')).offset().top;
        $('body, html').animate({scrollTop: top}, 1000);
    });
};
const B_TRUST_SECTION = () => {
    let $owl = $('._trustOwl');
    $owl.addClass('owl-carousel').owlCarousel({
        responsive: {
            0: {items: 1, dots: true, loop: true, mouseDrag: true, touchDrag: true},
            1260: {items: 1, mouseDrag: false, touchDrag: false}
        }
    });

    let goToStep = (index) => {
        goToSlide(index);
        if(parseInt(index) === 2){
            setTimeout(restartMiniSlider, 1000);
        }
    };

    $owl.on('changed.owl.carousel', (e) => {
        let index = e.page.index + 1;
        goToStep(index);
        autoplayStop();
    });

    $('._tabBtn').on('click', function () {
        let $el = $(this),
            selector = $el.attr('data-target'),
            index = selector.replace('._tab', ''),
            $target = $(selector),
            $all = $('._tab');

        $all.removeClass('active');
        $target.addClass('active');
        $('._tabBtn').removeClass('active');
        $el.addClass('active');

        goToStep(index);
        autoplayStop();
    });

    $('._smartPhoneDialogOwl').addClass('owl-carousel').owlCarousel({
        items: 1,
        dots: true
    });

    let $smartPhoneScreenOwl = $('._smartPhoneScreensOwl');
    window.goToSlide = (i) => {
        $smartPhoneScreenOwl.find('.owl-dot:nth-child('+i+')').trigger('click');
    };

    $smartPhoneScreenOwl.addClass('owl-carousel').owlCarousel({items: 1, dots: true});

    window.restartMiniSlider = () => {
        let interval,
            index = 1;

        interval = setInterval(() => {
            $('._smartPhoneDialogOwl').find('.owl-dot:nth-child('+index+')').trigger('click');
            index++;
            if(index === 5){
                clearInterval(interval);
            }
        }, 1000);
    };

    goToSlide(1);

    let $el = $('.trustSection'),
        prevIsVisible = false,
        timeout, restartTimeout;

    let autoplayStart = (index) => {
        let next = index < 4 ? index + 1 : 1,
            delays = {
                1: 3000,
                2: 8000,
                3: 3000,
                4: 4000
            };
        goToStep(index);
        timeout = setTimeout(() => {autoplayStart(next);}, delays[index]);
    };
    let autoplayStop = () => {
        clearTimeout(timeout);
        clearTimeout(restartTimeout);
        restartTimeout = setTimeout(() => { autoplayStart(0);}, 10000);
    };
    autoplayStart(0);
};
const B_STEPS = () => {
    $('._stepsOwl').addClass('owl-carousel').owlCarousel({
        responsive: {
            0: {items: 1, dots: true, loop: false, mouseDrag: true, touchDrag: true},
            1260: {items: 1, mouseDrag: false, touchDrag: false, loop: false,}
        }
    });
    $('._stepBtn').on('click', function () {
        let $el = $(this),
            $target = $($el.attr('data-target')),
            $all = $('._step');

        $all.removeClass('active');
        $target.addClass('active');
        $('._stepBtn').removeClass('active');
        $el.addClass('active');
    });
};
const B_COMPANIES = () => {
    $('._partnersOwl').addClass('owl-carousel').owlCarousel({
        responsive: {
            0: {items: 1, dots: true, loop: true, mouseDrag: true, touchDrag: true},
            768: {items: 5, mouseDrag: false, dots: false, touchDrag: false}
        }
    });
};
const B_FAQ = () => {
    PARALAX($('._bottomParalax'), {
        '._par3_1': {x: -.02, y: -.02},
        '._par3_2': {x: -.01, y: -.01},
        '._par3_3': {x: -.05, y: -.05},
        '._par3_4': {x: -.04, y: -.04},
    });
    $('._accordion .accordion__head').on('click', function () {
        let $head = $(this),
            $item = $head.parent('.accordion__item'),
            $collapse = $item.find('.accordion__collapse');

        if($item.hasClass('active')){
            $collapse.slideUp();
            $item.removeClass('active');
        }else{
            $collapse.slideDown();
            $item.addClass('active');
        }
    });
};
const B_FORM = () => {
    INIT_FOOTER_FORM();
};

$(function () {
    [B_FIRST_SCREEN, B_TRUST_SECTION, B_STEPS, B_COMPANIES, B_FAQ, B_FORM].forEach(function (func) {
        try {
            func();
        } catch (e) {
            console.log('Error in:' + func.name, e);
        }
    });
});