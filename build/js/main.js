'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

$(function () {
    var TEST = window.location.hostname == 'localhost';

    $('._stepsOwl').addClass('owl-carousel').owlCarousel({
        responsive: {
            0: { items: 1, dots: true, loop: false, mouseDrag: true, touchDrag: true },
            1260: { items: 1, mouseDrag: false, touchDrag: false, loop: false }
        }
    });

    $('._partnersOwl').addClass('owl-carousel').owlCarousel({
        responsive: {
            0: { items: 1, dots: true, loop: true, mouseDrag: true, touchDrag: true },
            768: { items: 5, mouseDrag: false, dots: false, touchDrag: false }
        }
    });

    var TRUST_SECTION = function TRUST_SECTION() {
        var $owl = $('._trustOwl');
        $owl.addClass('owl-carousel').owlCarousel({
            responsive: {
                0: { items: 1, dots: true, loop: true, mouseDrag: true, touchDrag: true },
                1260: { items: 1, mouseDrag: false, touchDrag: false }
            }
        });

        $owl.on('changed.owl.carousel', function (e) {
            var index = e.page.index + 1;
            goToSlide(index);
            if (index === 2) {
                setTimeout(restartMiniSlider, 1000);
            }
        });

        $('._tabBtn').on('click', function () {
            var $el = $(this),
                selector = $el.attr('data-target'),
                index = selector.replace('._tab', ''),
                $target = $(selector),
                $all = $('._tab');

            $all.removeClass('active');
            $target.addClass('active');
            $('._tabBtn').removeClass('active');
            $el.addClass('active');

            goToSlide(index);
            if (index === '2') {
                setTimeout(restartMiniSlider, 1000);
            }
        });

        $('._smartPhoneDialogOwl').addClass('owl-carousel').owlCarousel({
            items: 1,
            dots: true
        });

        var $smartPhoneScreenOwl = $('._smartPhoneScreensOwl');
        window.goToSlide = function (i) {
            $smartPhoneScreenOwl.find('.owl-dot:nth-child(' + i + ')').trigger('click');
        };

        $smartPhoneScreenOwl.addClass('owl-carousel').owlCarousel({ items: 1, dots: true });

        window.restartMiniSlider = function () {
            var interval = void 0,
                index = 1;

            interval = setInterval(function () {
                $('._smartPhoneDialogOwl').find('.owl-dot:nth-child(' + index + ')').trigger('click');
                index++;
                if (index === 5) {
                    clearInterval(interval);
                }
            }, 1000);
        };
    };

    TRUST_SECTION();

    $('._stepBtn').on('click', function () {
        var $el = $(this),
            $target = $($el.attr('data-target')),
            $all = $('._step');

        $all.removeClass('active');
        $target.addClass('active');
        $('._stepBtn').removeClass('active');
        $el.addClass('active');
    });

    $('._accordion .accordion__head').on('click', function () {
        var $head = $(this),
            $item = $head.parent('.accordion__item'),
            $collapse = $item.find('.accordion__collapse');

        if ($item.hasClass('active')) {
            $collapse.slideUp();
            $item.removeClass('active');
        } else {
            $collapse.slideDown();
            $item.addClass('active');
        }
    });

    var paralax = function paralax($el, set) {
        var pos = $el.offset(),
            width = $el.width(),
            height = $el.height(),
            transition = false,
            needPause = false,
            timeout = void 0;

        var move = function move(dx, dy) {
            Object.keys(set).forEach(function (selector) {
                var param = set[selector],
                    x = param.x ? param.x : .1,
                    y = param.y ? param.y : .1;
                $(selector).css({
                    transform: 'translate3d(' + dx * x + 'px, ' + dy * y + 'px, 0)',
                    transition: transition ? '.3s' : '0s'
                });
            });
        };
        var onMouseMove = function onMouseMove(e) {
            var ev = e.originalEvent,
                clientX = ev.clientX,
                clientY = ev.clientY,
                dx = clientX - (width / 2 + pos.left),
                dy = clientY - (height / 2 + pos.top);

            if (!needPause) {
                move(dx, dy);
            }
            needPause = transition;
        };
        var onResize = function onResize() {
            pos = $el.offset();
            width = $el.width();
            height = $el.height();
        };
        var onMouseLeave = function onMouseLeave() {
            transition = false;
            clearTimeout(timeout);
            Object.keys(set).forEach(function (selector) {
                $(selector).css({
                    transform: 'translate3d(0px, 0px, 0)',
                    transition: '.3s'
                });
            });
        };
        var onMouseOver = function onMouseOver() {
            transition = true;
            setTimeout(function () {
                transition = false;
            }, 300);
        };

        $el.on('mousemove', onMouseMove);
        $el.on('mouseleave', onMouseLeave);
        $el.on('mouseenter', onMouseOver);
        $(window).resize(onResize);
    };

    paralax($('._firstScreen'), {
        '._par1': { x: -.05, y: -.025 },
        '._par2': { x: -.025, y: -.0125 },
        '._par3': { x: -.025, y: -.0125 },
        '._par4': { x: -.04, y: -.02 },
        '._par2_1': { x: -.02, y: -.02 },
        '._par2_2': { x: -.01, y: -.01 },
        '._par2_3': { x: -.05, y: -.05 },
        '._par2_4': { x: -.04, y: -.04 }
    });

    paralax($('._bottomParalax'), {
        '._par3_1': { x: -.02, y: -.02 },
        '._par3_2': { x: -.01, y: -.01 },
        '._par3_3': { x: -.05, y: -.05 },
        '._par3_4': { x: -.04, y: -.04 }
    });

    var $bot = $('.firstScreenBot');
    setTimeout(function () {
        $bot.addClass('active');
        $('._botMessages').addClass('active');
    }, 500);

    var initForm = function initForm($el) {
        var $btn = $el.find('button'),
            $input = $el.find('input'),
            status = 'closed';

        var open = function open() {
            $el.addClass('active');
            //$input.focus();
            status = 'opened';
        };
        var close = function close() {
            if (status === 'success') return;

            $el.removeClass('active');
            status = 'closed';
        };
        var send = function send() {

            if ($input.val().length !== 18) return;

            $el.addClass('loading');
            status = 'loading';

            if (TEST) {
                setTimeout(function () {
                    $el.removeClass('loading');
                    $el.addClass('success');
                    status = 'success';
                    setTimeout(function () {
                        $input.val('');
                        $el.removeClass('active success');
                        status = 'closed';
                    }, 5000);
                }, 1000);
            } else {
                $.post('/form.php', { phone: $input.val() }, function (res) {
                    $el.removeClass('loading');
                    $el.addClass('success');
                    status = 'success';
                    setTimeout(function () {
                        $input.val('');
                        $el.removeClass('active success');
                        status = 'closed';
                    }, 5000);
                });
            }
        };
        var onClickBtn = function onClickBtn(e) {
            e.preventDefault();
            switch (status) {
                case 'closed':
                    open();
                    break;
                case 'opened':
                    send();
                    break;
            }
        };

        $input.mask("+7 (999) 999-99-99");

        $btn.on('click', onClickBtn);
        $el.on('click', function (e) {
            e.stopPropagation();
        });
        $('body').on('click', close);
    };

    $('._btnForm').each(function (i, el) {
        initForm($(el));
    });

    $('._anchor').on('click', function (e) {
        e.preventDefault();
        var top = $($(this).attr('href')).offset().top;
        $('body, html').animate({ scrollTop: top }, 1000);
    });

    var initFooterForm = function initFooterForm() {
        var $form = $('._form'),
            $btn = $form.find('button'),
            $input = $form.find('input');

        var isValid = function isValid() {
            return $input.val().length === 18;
        };

        var send = function send() {
            var $val = $input.val();

            $form.addClass('loading');

            if (TEST) {
                setTimeout(function () {
                    $form.removeClass('loading').addClass('success');
                    setTimeout(function () {
                        $input.val('');
                        $form.removeClass('success');
                    }, 3000);
                }, 2000);
            } else {
                $.post('/form.php', { 'phone': $val }, function (res) {
                    console.log(res);Œ;
                    $form.removeClass('loading').addClass('success');
                    setTimeout(function () {
                        $input.val('');
                        $form.removeClass('success');
                    }, 5000);
                });
            }
        };

        var onSubmitForm = function onSubmitForm(e) {
            e.preventDefault();

            if (isValid()) {
                send();
            }
        };

        $input.mask('+7 (999) 999-99-99');
        $form.on('submit', onSubmitForm);
    };

    initFooterForm();

    var BotMonolog = function () {
        function BotMonolog($parent) {
            _classCallCheck(this, BotMonolog);

            this.$root = $parent;

            this.speed = 500;
        }

        _createClass(BotMonolog, [{
            key: 'removeFirst',
            value: function removeFirst() {
                var _this = this;

                var $msg = $(this.$root.find('.botMessages__msgOuter')[0]);

                $msg.addClass('removed');
                $msg.css('transition', '0s');
                $msg.slideUp({ duration: this.speed });
                return new Promise(function (res) {
                    setTimeout(function () {
                        $msg.remove();res();
                    }, _this.speed + 100);
                });
            }
        }, {
            key: 'removeAll',
            value: function removeAll() {
                var _this2 = this;

                return new Promise(function (res) {
                    _this2.removePromise = res;_this2.removeOne();
                });
            }
        }, {
            key: 'removeOne',
            value: function removeOne() {
                var _this3 = this;

                var count = this.$root.find('.botMessages__msgOuter').length;
                if (count) {
                    this.removeFirst().then(function () {
                        _this3.removeOne();
                    });
                } else if (this.removePromise) {
                    this.removePromise();
                }
            }
        }, {
            key: 'calcMessagesHeightSum',
            value: function calcMessagesHeightSum() {
                var $msgs = this.$root.find('.botMessages__msgOuter'),
                    summ = 0;

                $msgs.each(function (i, el) {
                    summ += $(el).outerHeight();
                });

                return summ;
            }
        }, {
            key: 'addMessage',
            value: function addMessage(content) {
                var _this4 = this;

                var $msg = $(this.messageHtml(content));
                var sum = this.calcMessagesHeightSum(),
                    height = this.$root.parent().height();
                $msg.css({ transform: 'translateX(50%) translateY(' + (height / 2 - sum) + 'px)' });
                this.$root.append($msg);
                return new Promise(function (res) {
                    setTimeout(function () {

                        if (sum > _this4.$root.parent().height()) {
                            _this4.removeFirst();
                        }

                        $msg.css({ transform: 'none' });
                        $msg.removeClass('waiting');
                        setTimeout(function () {
                            res();
                        }, _this4.speed);
                    }, 100);
                });
            }
        }, {
            key: 'messageHtml',
            value: function messageHtml(content) {
                return '<div class="botMessages__msgOuter waiting" style="transition: ' + this.speed + 'ms">\n                        <div class="botMessages__msg" style="transition: ' + this.speed + 'ms">\n                            <div class="botMessage">' + content + '</div>\n                        </div>\n                    </div>';
            }
        }, {
            key: 'show',
            value: function show(arrMessages) {
                var _this5 = this;

                this.messages = arrMessages;
                return new Promise(function (res) {
                    _this5.addLoop(0, res);
                });
            }
        }, {
            key: 'addLoop',
            value: function addLoop(i, res) {
                var _this6 = this;

                if (!this.messages[i]) {
                    res();
                    return;
                }

                var text = this.messages[i].text,
                    delay = this.messages[i].delay ? this.messages[i].delay : 500;

                setTimeout(function () {
                    if (_this6.messages[i].clear) {
                        _this6.removeAll().then(function () {
                            _this6.addMessage(text).then(function () {
                                if (_this6.messages[i].onReady) {
                                    _this6.messages[i].onReady();
                                }
                                _this6.addLoop(i + 1, res);
                            });
                        });
                    } else {
                        _this6.addMessage(text).then(function () {
                            if (_this6.messages[i].onReady) {
                                _this6.messages[i].onReady();
                            }
                            _this6.addLoop(i + 1, res);
                        });
                    }
                }, delay);
            }
        }]);

        return BotMonolog;
    }();

    window.M = new BotMonolog($('._botMessages'));
    var show = function show(params) {
        return new Promise(function (res) {
            M.show(params).then(function () {
                res();
            });
        });
    };

    var loop = function loop(func, params) {
        func(params).then(function () {
            setTimeout(function () {
                loop(func, params);
            }, 2000);
        });
    };

    var buttons = '\n        <div class="interview _interview">\n            <div class="interview__circle">\n                <div class="interview__shapes _interviewDiagram">\n                    <span></span>\n                    <span><span class="circle"></span><span class="txt">\u041A\u0440\u0443\u0442\u043E!</span></span>\n                    <span><span class="circle"></span><span class="txt">\u0422\u0430\u043A \u0441\u0435\u0431\u0435</span></span>\n                </div>           \n                <div class="interview__numbers">\n                    <span></span>\n                    <span></span>\n                </div>\n            </div>\n        </div>\n    ';

    var diagramAnimation = function diagramAnimation() {
        setTimeout(function () {
            var $el = $('._interviewDiagram');
            $el.addClass('circle');
            setTimeout(function () {
                $el.addClass('complete');
            }, 1000);
        }, 1500);
    };
    var animateStars = function animateStars() {
        $('._stars').addClass('active');
    };
    var iphoneAnimate = function iphoneAnimate() {
        setTimeout(function () {
            $('._iphone').addClass('active');
        }, 500);
    };

    var starSVG = '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 41 40">\n      <path fill="#f8d548" d="M55,78L69,68l13,9L77,62l12-9H74L69,38,64,53H48l12,9Z" transform="translate(-48 -38)"/>\n    </svg>';

    var iphoneHTML = '\n        <div class="iphone _iphone">\n            <div class="iphone__circle">\n                <span></span>\n                <span></span>\n                <span></span>\n            </div>\n            <div class="iphone__coin"></div><div class="iphone__check"></div>\n            <div class="iphone__img"></div>\n        </div>\n    ';

    loop(show, [{ delay: 500, text: 'Привет! Я общаюсь с клиентами и помогаю им сделать покупку.', clear: true }, { delay: 500, text: 'Оперативно отвечаю на вопросы и всегда рад клиенту.' }, { delay: 500, text: 'Сообщаю о статусе заказа стикерами в стиле вашей компании.' }, { delay: 500, text: '<img src="img/1.png"/>' }, { delay: 2500, text: 'Провожу опросы и показываю статистику ответов.', clear: true }, { delay: 500, text: buttons, onReady: diagramAnimation }, { delay: 4500, text: 'Отправляю персональные акции.', clear: true }, { delay: 500, text: '<em>Мы знаем, как Вы любите Тайланд. Возможно Вам понравится здесь</em> ☺' }, { delay: 500, text: '<img src="img/hotel.jpg" style="width: 100%"/>' }, { delay: 2500, text: 'Собираю отзывы клиентов.', clear: true }, { delay: 500, text: '<div class="stars _stars">' + starSVG + starSVG + starSVG + starSVG + starSVG + '</div>', onReady: animateStars }, { delay: 2500, text: 'Привлекательно показываю ассортимент товаров и услуг.', clear: true }, { delay: 500, text: '<img src="img/product_1.png" class="product"/><img class="product" src="img/product_2.png"/>' }, { delay: 5000, text: 'Принимаю оплаты!', clear: true }, { delay: 500, text: iphoneHTML, onReady: iphoneAnimate }]);
});