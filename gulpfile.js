var gulp         = require('gulp'),
    $            = require('gulp-load-plugins')(),
    sass         = require('gulp-sass'),
    browserSync  = require('browser-sync'),
    concat       = require('gulp-concat'),
    uglify       = require('gulp-uglifyjs'),
    cssnano      = require('gulp-cssnano'),
    del          = require('del'),
    cache        = require('gulp-cache'),
    urlAdjuster  = require('gulp-css-url-adjuster'),
    concatCss    = require('gulp-concat-css'),
    autoprefixer = require('gulp-autoprefixer'),
    babel = require('gulp-babel');

const versionConfig = {
    'value': '%MDS%',
    'append': {
        'key': 'v',
        'to': ['css', 'js'],
    },
};

gulp.task('sass', function() {
    return gulp.src('public/sass/*.sass')
        .pipe(sass())
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(gulp.dest('public/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('scripts', function(){
    return gulp.src([
            'public/libs/jquery.min.js',
            'public/libs/owl.carousel.min.js'
        ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

gulp.task('css-libs', ['sass'], function(){
    return gulp.src('public/css/libs.css')
        .pipe(concatCss("libs.min.css"))
        .pipe(cssnano())
        .pipe(gulp.dest('public/css'));
});

gulp.task('clean', function(){
    return del.sync('build');
});

gulp.task('clear', function(){
    return cache.clearAll();
});

gulp.task('img', function(){
    return gulp.src('public/img/**/*')
        .pipe(gulp.dest('build/img'));
});

gulp.task('browser-sync', function(){
    browserSync({
        server: {
            baseDir: 'public'
        },
        notify: false
    });
});


gulp.task('watch', ['browser-sync', 'css-libs', 'scripts'], function() {
    gulp.watch('public/sass/**/*.sass', ['sass']);
    gulp.watch('public/*.html', browserSync.reload);
    gulp.watch('public/js/**/*.js', browserSync.reload);
});

gulp.task('build', ['clean', 'sass', 'scripts', 'img'], function(){
    gulp.src(['public/css/main.css', 'public/css/libs.min.css'])
        .pipe(urlAdjuster({ replace:  ['../../img/', '/img/']}))
        .pipe(urlAdjuster({ replace:  ['../img/', '/img/']}))
        .pipe(urlAdjuster({ replace:  ['/img/', '../img/']}))
        .pipe(gulp.dest('build/css'));

    gulp.src('public/fonts/**/*')
        .pipe(gulp.dest('build/fonts'));

    gulp.src('public/js/**/*')
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(gulp.dest('build/js'));

    gulp.src('public/*.html')
        .pipe($.versionNumber(versionConfig))
        .pipe(gulp.dest('build'));

    gulp.src('public/favicon/**/*')
        .pipe(gulp.dest('build/favicon'));
    gulp.src('public/svg/**/*')
        .pipe(gulp.dest('build/svg'));

    gulp.src(['public/*.png', 'public/*.ico', 'public/*.json'])
        .pipe(gulp.dest('build'));
});

gulp.task('default', ['watch']);